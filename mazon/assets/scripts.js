$(document).ready(function(){
	//hide the banner so that users don't see the images until the slider is initialized
	$('.banner').hide();

	var callUnslider = function(){
		var height = $('.banner ul li img:first').height();
		console.log('banner height is: ' + height);
		$('.banner').show();
			var unslider = $('.banner').unslider({
			    speed : 500,                //  The speed to animate each slide (in milliseconds)
			    delay : 5000,               //  The delay between slide animations (in milliseconds)
			    complete : function() {},   //  A function that gets called after every slide animation
			    keys : false,                //  Enable keyboard (left, right) arrow shortcuts
			    fluid : true,               //  Support responsive design. May break non-responsive designs
			    arrows: false,
			    dots: true
			});
		}

	$('.unslider-arrow').click(function() {
	var fn = this.className.split(' ')[1];
	//  Either do unslider.data('unslider').next() or .prev() depending on the className
	unslider.data('unslider')[fn]();
	});

	//if IE caches the images, it will never fire a load event for it
	//here we add a query string to the first banner image's src attribute, so that IE will not cache the image.

	//get the image's src attribute
	var source = $('.banner ul li img').first().attr("src");
	//add the date as a query string to the image's src attribute, so that every time the page is refreshed, IE will reload it and fire the load event
	$('.banner ul li img').first().attr("src",source + "?" + new Date().getTime());
	//initilize the unslider plugin, but only after the first image loads

	$('.banner ul li img').first().load(function(){callUnslider ()});
});


/*Height resize adjustment for tablet-only 'Learn about our work' box */
$(document).ready(function() {
	 var win = $(this);
		// Height source
	 	var tab_sidebar = $('.content-block .sub-sidebar-1');
	 	var sidebar = $('.sidebar-right .sub-sidebar-1');
	 	// height target
	 	var intro = $('.content-block .intro-text');
	 	// Set divs to same height
	 	if (win.width() > 756 && win.width() <= 1020) {
	 		console.log("loading on tablet");
	 		intro.height(tab_sidebar.css( "height" ));
	 	} 
	$(window).on('resize', function(){
		 if (win.width() > 756 && win.width() <= 1020 ) {
		 	// console.log('setting height on resize');
		 	intro.height(tab_sidebar.css( "height" ));
		 } else if (win.width() > 1020) {
		 	console.log("back to full width");
		 	intro.height(131);
		 } else if (win.width() <= 756) {
		 	console.log('going to phone');
		 	console.log('css height: ' +sidebar.css("height"))
		 	console.log('height: ' +sidebar.height())
		 	intro.height(sidebar.height()*.8);
		 }
	});

});
