(function($, window, document, undefined) {
	$(document).ready(function() {
		var mapHover = {

			init: function() {
				this.bindEvents();
			}, 

			bindEvents: function() {			
				$('.roadmap-listing').on({
					mouseenter: function() {
						var selector = ('#' + $(this).data('color'));
						$('.map-container').find(selector + '-map-overlay').addClass('active');
					},
					mouseleave: function() {
						var selector = ('#' + $(this).data('color'));
						$('.map-container').find(selector + '-map-overlay').removeClass('active');
					},
					click: function() {
						var selector = ('#' + $(this).data('color'));
						$('.map-container').find(selector).addClass('active');
					}
				}, '.section');
			}
		};
		mapHover.init();
	});
}(jQuery, window, document)); 	