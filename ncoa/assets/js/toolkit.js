(function($, window, document, undefined) {
  'use strict';
  $(document).ready(function() {
    var tk = {
      init: function() {
        this.cacheDom();
        this.bindEvents();
      }, 
      cacheDom: function() {
        this.$toolkit = $('#toolkit');
      },

      bindEvents: function() {    
        this.$toolkit.find('.tk-menu-3 a').on('click', function(e) {
          e.preventDefault();
          tk.toggleContent($(this));
        });

        this.$toolkit.find('.tk-menu-2 a').on('click', function(e) {
          e.preventDefault();
          tk.toggleItemSubsection($(this));
        });

         this.$toolkit.find('.tk-menu-1 a').on('click', function(e) {
          e.preventDefault();
          tk.toggleSection($(this));
        });

        this.$toolkit.find('.tk-item-title').on('click', function(e) {
          tk.toggleItem($(this));
        });
      }, 

      _removeActive: function(sel) {
        this.$toolkit.find(sel).removeClass('active');
      },

      _getWidth: function() {
        this.width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
      },
      _scrollTo: function(sel, allWidths) {
        this._getWidth();
        // allWidths boolean overrides width check
        if (this.width > 768 && !allWidths) {
            return;
          } else {
            var scrollSel = (typeof sel === 'object') ? sel : $(sel);
            $('html, body').animate({
            scrollTop: scrollSel.offset().top
            }, 1000);
          }
      },


      toggleSection: function($sectionLink) {
        this._removeActive('.tk-menu-1 a');
        $sectionLink.addClass('active');
        var selector = ('#' + $sectionLink.data('topic'));
        this._removeActive('.tk-item-group');
        this.$toolkit.find(selector).addClass('active');
        this._scrollTo('.tk-menu-1');
      },

      toggleItem: function($itemElem) {
        if ($itemElem.parent().hasClass('active')) {
          this._removeActive('.tk-item');
        } else {
          this._removeActive('.tk-item');
          $itemElem.parent().addClass('active');
        }
         this._scrollTo($itemElem, true);
      },

      toggleContent: function($sourceEl) {
        $sourceEl.closest('.item-subsection').find('.tk-content').removeClass('active');
        if (!$sourceEl) {
          return;
        } else {
          //Switch Menu Active Class
          $sourceEl.closest('.tk-menu-3').find('a').removeClass('active');
          $sourceEl.addClass('active');
          //Find and show content area based on data attr in clicked <a>
          var selector = ('#' + $sourceEl.data('topic'));
          this.$toolkit.find(selector).addClass('active');
          this._scrollTo(selector);
        }
      },
      toggleItemSubsection: function($menuTwoElem) {
        $menuTwoElem.closest('.container').find('.item-subsection').removeClass('active');
         if (!$menuTwoElem) {
          return;
        } else {
          this._removeActive('.tk-menu-2 a');
          $menuTwoElem.toggleClass('active');
          var selector = ('#' + $menuTwoElem.data('topic'));
          this.$toolkit.find(selector).addClass('active');
          this._scrollTo(selector);
        }
      }
    };
    tk.init();
  });
}(jQuery, window, document));   