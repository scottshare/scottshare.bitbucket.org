(function($, window, document, undefined) {
	$(document).ready(function() {
		var $left = $('.left');

		$('.listing').on({
			mouseenter: function() {
				var selector = ('#' + $(this).html());
				$left.find(selector).addClass('active')
			},
			mouseleave: function() {
				var selector = ('#' + $(this).html());
				$left.find(selector).removeClass('active')
			},
			click: function() {
				var selector = ('#' + $(this).html());
				$left.find(selector).addClass('active')
			}
		}, '.section');


	});
}(jQuery, window, document)); 	