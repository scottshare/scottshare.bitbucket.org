$(document).ready(function() {
	
	function add_active_class(toggle,target) {
        $(toggle).click( function () {
            $(this).toggleClass('active');	
            $(target).toggleClass('active');
        });
	}
    add_active_class('#js-search-toggle','#js-search-form');
    add_active_class('#js-menu-toggle','.main');

});


